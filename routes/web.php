<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return redirect('login'); });
Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();

Route::group(['namespace' => 'User'], function(){
    // Routing Group Menu Users
    Route::group(['prefix' => 'user'], function(){
        Route::get('/', 'UserController@index');
        Route::post('/', 'UserController@store');
        Route::get('/create', 'UserController@create');
        Route::get('/edit/{id}', 'UserController@edit');
        Route::put('/update/{id}', 'UserController@update');
        Route::delete('/delete/{id}', 'UserController@delete');
    });

    // Routing Group Menu Level
    Route::group(['prefix' => 'level'], function(){
        Route::get('/', 'LevelController@index');
        Route::post('/', 'LevelController@store');
        Route::get('/create', 'LevelController@create');
        Route::get('/edit/{id}', 'LevelController@edit');
        Route::put('/update/{id}', 'LevelController@update');
        Route::delete('/delete/{id}', 'LevelController@delete');
    });
});

// Routing Group Menu System
Route::group(['prefix' => 'system', 'namespace' => 'System'], function(){

    // Routing Group Menu Module
    Route::group(['prefix' => 'module'], function(){
        Route::get('/', 'ModuleController@index');
        Route::post('/', 'ModuleController@store');
        Route::get('/create', 'ModuleController@create');
        Route::get('/edit/{id}', 'ModuleController@edit');
        Route::put('/update/{id}', 'ModuleController@update');
        Route::delete('/delete/{id}', 'ModuleController@delete');
    });

    // Routing Group Menu Task
    Route::group(['prefix' => 'task'], function(){
        Route::get('/', 'TaskController@index');
        Route::post('/', 'TaskController@store');
        Route::get('/create', 'TaskController@create');
        Route::get('/edit/{id}', 'TaskController@edit');
        Route::put('/update/{id}', 'TaskController@update');
        Route::delete('/delete/{id}', 'TaskController@delete');
    });

    // Routing Group Menu
    Route::group(['prefix' => 'menu'], function(){
        Route::get('/', 'MenuController@index');
        Route::post('/', 'MenuController@store');
        Route::get('/create', 'MenuController@create');
        Route::get('/edit/{id}', 'MenuController@edit');
        Route::put('/update/{id}', 'MenuController@update');
        Route::delete('/delete/{id}', 'MenuController@delete');
    });

    // Routing Group Menu Role
    Route::group(['prefix' => 'role'], function(){
        Route::get('/', 'RoleController@index');
        Route::get('/edit/{id}', 'RoleController@edit');
        Route::post('/update', 'RoleController@update');
    });
});


Route::group(['namespace' => 'Master'], function(){
    // Routing Master Quisioner
    Route::group(['prefix' => 'quisioner'], function(){
        Route::get('/', 'QuisionerController@index');
        Route::post('/', 'QuisionerController@store');
        Route::get('/create', 'QuisionerController@create');
        Route::get('/edit/{id}', 'QuisionerController@edit');
        Route::get('/show/{id}', 'QuisionerController@show');
        Route::put('/update/{id}', 'QuisionerController@update');
        Route::delete('/delete/{id}', 'QuisionerController@delete');
    });
});


Route::group(['prefix' => 'agreement', 'namespace' => 'Document'], function(){
    // Routing Agreement Mutual
    Route::group(['prefix' => 'mutual'], function(){
        Route::get('/', 'MutualController@index');
        Route::post('/', 'MutualController@store');
        Route::get('/create', 'MutualController@create');
        Route::get('/edit/{id}', 'MutualController@edit');
        Route::get('/show/{id}', 'MutualController@show');
        Route::put('/update/{id}', 'MutualController@update');
        Route::delete('/delete/{id}', 'MutualController@delete');
    });

    // Routing Agreement Collective
    Route::group(['prefix' => 'collective'], function(){
        Route::get('/', 'CollectiveController@index');
        Route::post('/', 'CollectiveController@store');
        Route::get('/create', 'CollectiveController@create');
        Route::get('/edit/{id}', 'CollectiveController@edit');
        Route::get('/show/{id}', 'CollectiveController@show');
        Route::put('/update/{id}', 'CollectiveController@update');
        Route::delete('/delete/{id}', 'CollectiveController@delete');
    });
});

Route::group(['prefix' => 'quisioner', 'namespace' => 'Quisioner'], function(){
    // Routing Agreement Mutual
    Route::group(['prefix' => 'mutual'], function(){
        Route::get('/', 'MutualController@index');
        Route::post('/', 'MutualController@store');
        Route::get('/create', 'MutualController@create');
        Route::get('/edit/{id}', 'MutualController@edit');
        Route::get('/show/{id}', 'MutualController@show');
        Route::put('/update/{id}', 'MutualController@update');
        Route::delete('/delete/{id}', 'MutualController@delete');
    });

    // Routing Agreement Collective
    Route::group(['prefix' => 'collective'], function(){
        Route::get('/', 'CollectiveController@index');
        Route::post('/', 'CollectiveController@store');
        Route::get('/create', 'CollectiveController@create');
        Route::get('/edit/{id}', 'CollectiveController@edit');
        Route::get('/show/{id}', 'CollectiveController@show');
        Route::put('/update/{id}', 'CollectiveController@update');
        Route::delete('/delete/{id}', 'CollectiveController@delete');
    });
});