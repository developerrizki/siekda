<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tasks 		= DB::table('sys_tasks')->get();
        $levels 	= DB::table('user_level')->get();

        foreach ($levels as $key => $level) 
        {
        	foreach ($tasks as $key => $task) {
        		DB::table('sys_roles')->insert([
        			"tasks_id" 		=> $task->tasks_id,
        			"users_id" 		=> $level->level_id
	        	]);
        	}
        }
    }
}
