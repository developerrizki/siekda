<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = array([
            'name' => 'Super Admin',
            'username' => 'devadm',
            'email' => 'devadm@siekda.com',
            'password' => bcrypt('devadmin123'),
            'level_id' => 1
        ],
        [
            'name' => 'Admin',
            'username' => 'admin',
            'email' => 'admin@siekda.com',
            'password' => bcrypt('123456789'),
            'level_id' => 2
        ]);

        for ($i=0; $i < sizeof($user); $i++) { 
            DB::table('users')->insert($user[$i]);
        }
    }
}
