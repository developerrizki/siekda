<?php

use Illuminate\Database\Seeder;

class UserLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level = ["Super Admin","Admin"];

        for($i=0; $i < sizeof($level); $i++)
        {
            DB::table('user_level')->insert([
                'level_id' => $i+1,
                'level' => $level[$i]
            ]);
        }
    }
}
