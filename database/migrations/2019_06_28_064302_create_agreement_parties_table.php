<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementPartiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreement_parties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agreement_id')->unsigned()->nullable();
            $table->string('name',200)->nullable();
            $table->integer('user_created')->unsigned()->nullable();
            $table->integer('user_updated')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('agreement_id')->references('id')->on('agreements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreement_parties');
    }
}
