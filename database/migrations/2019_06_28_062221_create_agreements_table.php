<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number', 100)->nullable();
            $table->text('name')->nullable();
            $table->mediumText('summary')->nullable();
            $table->text('periode')->nullable();
            $table->string('opd_initiator', 200)->nullable();
            $table->text('information')->nullable();
            $table->text('file')->nullable();
            $table->datetime('due_date')->nullable();
            $table->integer('is_mutual')->default(0)->comment('0 : Mutual Agreement, 1: Collective Agreement');
            $table->integer('user_created')->unsigned()->nullable();
            $table->integer('user_updated')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('user_created')->references('id')->on('users');
            $table->foreign('user_updated')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreements');
    }
}
