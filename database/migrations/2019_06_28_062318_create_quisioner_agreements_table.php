<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuisionerAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quisioner_agreements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quisioner_id')->unsigned()->nullable();
            $table->integer('agreement_id')->unsigned()->nullable();
            $table->integer('poin_id')->unsigned()->nullable();
            $table->integer('user_created')->unsigned()->nullable();
            $table->integer('user_updated')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('quisioner_id')->references('id')->on('quisioners');
            $table->foreign('agreement_id')->references('id')->on('agreements');
            $table->foreign('poin_id')->references('id')->on('quisioner_poins');
            $table->foreign('user_created')->references('id')->on('users');
            $table->foreign('user_updated')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quisioner_agreements');
    }
}
