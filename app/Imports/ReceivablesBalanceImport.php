<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use App\Model\Receivables\Receivables;

class ReceivablesBalanceImport implements ToCollection, WithHeadingRow, SkipsOnError
{

	use Importable, SkipsErrors;

	protected $errors = [];

    public function collection(Collection $rows)
    {
		// Validator::make($rows->toArray(), [
		// 	'*.nama_customer' => function($attribute, $value, $onFailure) {
		// 		echo json_encode('*.no_pembelian');
		// 		die;
		// 		if ($value !== 'Patrick Brouwers') {
		// 			$onFailure('Name is not Patrick Brouwers');
		// 		} else {

		// 		}
		// 	},
		// ])->validate();

		// die;

    	foreach ($rows as $row)
        {
			$balance  = Receivables::where('code', $row['no_pembelian'])->first();

			if ($balance) {
				$update = Receivables::where('code', $row['no_pembelian'])
          		->where('value_of_receivable', $row['nilai_piutang'])
          		->where('balance_of_receivable', $row['saldo_piutang'])
          		->update([
          			'customer_name'     	=> $row['nama_customer'],
					'invoice_date'    		=> $row['tanggal_invoice'],
					'supplier_name'			=> \Auth::user()->name,
					'code' 					=> $row['no_pembelian'],
					'is_factory'			=> \Auth::user()->level_id,
					'value_of_receivable' 	=> $row['nilai_piutang'],
					'balance_of_receivable' => $row['saldo_piutang'],
          		]);

          		if (!$update)
          			$this->errors[] = 'Piutang atas nama ' . $row['nama_customer'] . ' sudah melakukan pembayaran';
			} else {
				if ($row['nilai_piutang'] != $row['saldo_piutang']) {
					$this->errors[] = 'Piutang atas nama ' . $row['nama_customer'] . ' sudah melakukan pembayaran';
					continue;
				}

	            Receivables::create([
					'customer_name'     	=> $row['nama_customer'],
					'invoice_date'    		=> $row['tanggal_invoice'],
					'supplier_name'			=> \Auth::user()->name,
					'code' 					=> $row['no_pembelian'],
					'is_factory'			=> \Auth::user()->level_id,
					'value_of_receivable' 	=> $row['nilai_piutang'],
					'balance_of_receivable' => $row['saldo_piutang'],
	            ]);
			}
        }

        if (count($this->errors) > 0) {
        	throw \Illuminate\Validation\ValidationException::withMessages($this->errors);
        }
    }

	/**
     * @param \Throwable $e
     */
    public function onError(\Throwable $e)
    {
        
    }
}
