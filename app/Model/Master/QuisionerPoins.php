<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class QuisionerPoins extends Model
{
    protected $table = "quisioner_poins";

    public function quisioner()
    {
        return $this->belongsTo('App\Model\Master\Qusioners', 'quisioner_id');
    }
}
