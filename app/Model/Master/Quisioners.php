<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Quisioners extends Model
{
    protected $table = "quisioners";

    public function getDataWithPaginate($request = null, $limit = null)
    {
        $quisioner = Quisioners::orderBy('title','ASC');

        if($request->has('key') && $request->has('value')) {
            $quisioner->where($request->key, 'like', '%'.$request->value.'%');
        }

        return $quisioner->paginate($limit);
    }

    public function poins()
    {
        return $this->hasMany('App\Model\Master\QuisionerPoins','quisioner_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User','user_created');
    }
}
