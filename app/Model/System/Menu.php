<?php

namespace App\Model\System;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = "sys_menus";
    protected $primaryKey = "menu_id";

    public function getModule()
    {
        return $this->belongsTo('App\Model\System\Module','module_id','modules_id');
    }

    public function getMenu()
    {
        return $this->belongsTo('App\Model\System\Menu','menu_parent','menu_id');
    }

    public function getDataWithPaginate($request = null, $limit = null)
    {
        $menu = Menu::orderBy('menu_id','ASC');

        if($request->has('key') && $request->has('param')) {
            $menu->where($request->param, 'like', '%'.$request->key.'%');
        }

        return $menu->paginate($limit);
    }
}
