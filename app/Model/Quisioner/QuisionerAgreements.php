<?php

namespace App\Model\Quisioner;

use Illuminate\Database\Eloquent\Model;

class QuisionerAgreements extends Model
{
    protected $table = "quisioner_agreements";

    public function getDataWithPaginate($request = null, $limit = null, $is_mutual = null)
    {
        $quisioner = QuisionerAgreements::select([
                            '*','quisioner_agreements.id as qa_id'
                        ])
                        ->orderBy('quisioner_agreements.quisioner_id','ASC')
                        ->where('agreements.is_mutual', $is_mutual)
                        ->join('agreements','quisioner_agreements.agreement_id','=','agreements.id')
                        ->join('quisioners','quisioner_agreements.quisioner_id','=','quisioners.id')
                        ->join('quisioner_poins','quisioner_agreements.poin_id','=','quisioner_poins.id');

        if($request->has('key') && $request->has('value')) {
            $quisioner->where($request->key, 'like', '%'.$request->value.'%');
        }

        return $quisioner->paginate($limit);
    }

    public function quisioner()
    {
        return $this->belongsTo('App\Model\Master\Quisioners','quisioner_id');
    }

    public function poins()
    {
        return $this->belongsTo('App\Model\Master\QuisionerPoins','poin_id');
    }

    public function agreement()
    {
        return $this->belongsTo('App\Model\Document\Agreements','agreement_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User','user_created');
    }
}
