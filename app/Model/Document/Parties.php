<?php

namespace App\Model\Document;

use Illuminate\Database\Eloquent\Model;

class Parties extends Model
{
    protected $table = "agreement_parties";

    public function agreement()
    {
        return $this->belongsTo('App\Model\Document\Agreements', 'agreement_id');
    }
}
