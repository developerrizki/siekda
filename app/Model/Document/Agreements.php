<?php

namespace App\Model\Document;

use Illuminate\Database\Eloquent\Model;

class Agreements extends Model
{
    protected $table = "agreements";

    public function getDataWithPaginate($request = null, $limit = null, $is_mutual = null)
    {
        $agreements = Agreements::orderBy('name','ASC')->where('is_mutual', $is_mutual);

        if($request->has('key') && $request->has('value')) {
            $agreements->where($request->key, 'like', '%'.$request->value.'%');
        }

        return $agreements->paginate($limit);
    }

    public function parties()
    {
        return $this->hasMany('App\Model\Document\Parties','agreement_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User','user_created');
    }
}
