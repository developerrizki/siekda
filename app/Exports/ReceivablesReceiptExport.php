<?php
namespace App\Exports;

use Illuminate\Http\Request;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;

use App\Model\Debt\Debts;

class ReceivablesReceiptExport implements FromQuery, WithHeadings
{
    public function forCustomerName(string $name = null)
    {
        $this->customer_name = $name;
        return $this;
    }

    public function forStart(string $start)
    {
        $this->start = $start;

        return $this;
    }

    public function forFinish(string $finish)
    {
        $this->finish = $finish;

        return $this;
    }

    public function headings(): array
    {
        return [
            'No Pembayaran',
            'No Penerimaan',
            'Nama Customer',
            'Tanggal Penerimaan'
        ];
    }

    use Exportable;

    public function query()
    {
        $start     = date_format(date_create($this->start), 'Y-m-d');
        $finish    = date_format(date_create($this->finish), 'Y-m-d');

        $result = Debts::query()->select(
            'debts.code as `No Pembayaran`',
            'receivable_pays.no_receipt as `No Penerimaan`',
            'debts.customer_name as `Nama Customer`',
            'receivable_pays.created_at'
        )
        ->join('receivable_pays', 'receivable_pays.code', '=', 'debts.code')
        ->orderBy('debts.id', 'desc')
        ->whereBetween(\DB::raw('DATE(debts.created_at)'),[$start, $finish]);

        if ($this->customer_name)
            $result->where('debts.customer_name', 'like', '%' . $this->customer_name . '%');

        return $result;
    }
}
?>
