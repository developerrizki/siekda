<?php

namespace App\Http\Controllers\Quisioner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Quisioner\StoreQuisionerAgreements;

use DB;
use Auth;
use App\Model\Document\Agreements;
use App\Model\Master\Quisioners;
use App\Model\Quisioner\QuisionerAgreements;

class MutualController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit            = 50;
        $this->is_mutual        = 1;
        $this->modelQAgreement   = new QuisionerAgreements;
    }

    public function index(Request $request)
    {
        $data = $this->modelQAgreement->getDataWithPaginate($request, $this->limit, $this->is_mutual);
        return view('quisioner.mutual.index', compact('data'));
    }

    public function create()
    {
        $agreements = Agreements::orderBy('name','ASC')->where('is_mutual', $this->is_mutual)->get();
        $quisioners = Quisioners::orderBy('title','ASC')->get();
        
        return view('quisioner.mutual.create', compact('agreements','quisioners'));
    }

    public function store(StoreQuisionerAgreements $request)
    {
        $agreement_id   = $request->agreement_id;
        $quisioner_id   = $request->quisioner_id;
        $poin_id        = $request->poin_id;
        $user_created   = Auth::user()->id;

        DB::beginTransaction();

            for ($i=0; $i < sizeof($quisioner_id); $i++) { 
                $quisioner_agreements                   = new QuisionerAgreements;
                $quisioner_agreements->agreement_id     = $agreement_id;
                $quisioner_agreements->quisioner_id     = $quisioner_id[$i];
                $quisioner_agreements->poin_id          = $poin_id[$i];
                $quisioner_agreements->user_created     = $user_created;
                $quisioner_agreements->save();
            }
        
        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Kuisioner perjanjian kerjasama berhasil ditambahkan');

        return redirect('quisioner/mutual');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= QuisionerAgreements::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kuisioner perjanjian kerjasama berhasil di hapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Kuisioner perjanjian kerjasama gagal di hapus');
        }

    	return redirect()->back();
    }
}
