<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Master\StoreQuisioner;
use App\Http\Requests\Master\UpdateQuisioner;

use DB;
use Auth;
use App\Model\Master\Quisioners;
use App\Model\Master\QuisionerPoins as Poin;

class QuisionerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit            = 50;
        $this->modelPoin        = new Poin;
        $this->modelQuisioner   = new Quisioners;
    }

    public function index(Request $request)
    {
        $data = $this->modelQuisioner->getDataWithPaginate($request, $this->limit);
        return view('master.quisioner.index', compact('data'));
    }

    public function create()
    {
        return view('master.quisioner.create');
    }

    public function store(StoreQuisioner $request)
    {
        //Param Quisioner
        $title       = $request->title;

        //Param Array Poin Quisioner
        $poin        = $request->poin;

        //User
        $user_created = Auth::user()->id;

        DB::beginTransaction();

            $quisioner                  = new Quisioners;
            $quisioner->title           = $title;
            $quisioner->user_created    = $user_created;
            $quisioner->save();

            for ($i=0; $i < sizeof($poin); $i++) { 
                $poins                  = new Poin;
                $poins->quisioner_id    = $quisioner->id;
                $poins->poin            = $poin[$i];
                $poins->user_created    = $user_created;
                $poins->save();
            }

        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Kuisioner berhasil ditambahkan');

        return redirect('quisioner');
    }

    public function edit($id)
    {
        $data = Quisioners::find($id);
        return view('master.quisioner.edit', compact('data'));
    }

    public function update(UpdateQuisioner $request, $id)
    {
        //Param Quisioner
        $quisioner_id = $id;
        $title        = $request->title;

        //Param Array Poin Quisioner
        $poin        = $request->poin;
        $poin_id     = $request->poin_id;

        //User
        $user_created = Auth::user()->id;

        DB::beginTransaction();

            $quisioner                  = Quisioners::find($id);
            $quisioner->title           = $title;
            $quisioner->user_created    = $user_created;
            $quisioner->save();

            for ($i=0; $i < sizeof($poin); $i++) { 
                $poins                  = isset($poin_id[$i]) ? Poin::find($poin_id[$i]) : new Poin;
                $poins->quisioner_id    = $quisioner->id;
                $poins->poin            = $poin[$i];
                $poins->user_created    = $user_created;
                $poins->save();
            }

        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Kuisioner berhasil diubah');

        return redirect('quisioner');
    }

    public function show($id)
    {
        $data = Quisioners::find($id);
        return view('master.quisioner.show', compact('data'));
    }

    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
            
            Poin::where('quisioner_id', $id)->delete();
            Quisioners::findOrFail($id)->delete();

        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Kuisioner berhasil dihapus');

    	return redirect()->back();
    }

}
