<?php

namespace App\Http\Controllers\Document;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Document\StoreAgreements;
use App\Http\Requests\Document\UpdateAgreements;
use Illuminate\Support\Facades\Storage;

use DB;
use Auth;
use App\Model\Document\Agreements;
use App\Model\Document\Parties;

class CollectiveController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->limit            = 50;
        $this->is_mutual        = 0;
        $this->modelAgreement   = new Agreements;
    }

    public function index(Request $request)
    {
        $data = $this->modelAgreement->getDataWithPaginate($request, $this->limit, $this->is_mutual);
        return view('document.collective.index', compact('data'));
    }

    public function create()
    {
        return view('document.collective.create');
    }

    public function store(StoreAgreements $request)
    {
        //Param Agreement
        $number             = $request->number;
        $name               = $request->name;
        $summary            = $request->summary;
        $periode            = $request->periode;
        $opd_initiator      = $request->opd_initiator;
        $information        = $request->information;
        $due_date           = $request->due_date;

        //Param Array Parties
        $name_of_parties    = $request->name_of_parties;

        //User
        $user_created = Auth::user()->id;

        DB::beginTransaction();

            $agreement                  = new Agreements;
            $agreement->number          = $number;
            $agreement->name            = $name;
            $agreement->summary         = $summary;
            $agreement->periode         = $periode;
            $agreement->opd_initiator   = $opd_initiator;
            $agreement->information     = $information;
            $agreement->due_date        = $due_date;
            $agreement->is_mutual       = 0;
            $agreement->user_created    = $user_created;

            if($request->hasFile('file')){
                $files = $_FILES['file']['name'];
                $ext = pathinfo($files, PATHINFO_EXTENSION);
                
                $file_name              = 'collectives_'.time().'.'.$ext;
                $request->file('file')->storeAs('public/documents/collectives', $file_name);
                
                $agreement->file        = $file_name;
            }

            $agreement->save();

            for ($i=0; $i < sizeof($name_of_parties); $i++) { 
                if($name_of_parties[$i] != null)
                {
                    $parties                  = new Parties;
                    $parties->agreement_id    = $agreement->id;
                    $parties->name            = $name_of_parties[$i];
                    $parties->user_created    = $user_created;
                    $parties->save();
                }
            }

        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Kesepakatan bersama berhasil ditambahkan');

        return redirect('agreement/collective');
    }

    public function edit($id)
    {
        $data = Agreements::find($id);
        return view('document.collective.edit', compact('data'));
    }

    public function update(UpdateAgreements $request, $id)
    {
        //Param Agreement
        $number             = $request->number;
        $name               = $request->name;
        $summary            = $request->summary;
        $periode            = $request->periode;
        $opd_initiator      = $request->opd_initiator;
        $information        = $request->information;
        $due_date           = $request->due_date;

        //Param Array Parties
        $parties_id         = $request->parties_id;
        $name_of_parties    = $request->name_of_parties;

        //User
        $user_created = Auth::user()->id;

        DB::beginTransaction();

            $agreement                  = Agreements::find($id);
            $agreement->number          = $number;
            $agreement->name            = $name;
            $agreement->summary         = $summary;
            $agreement->periode         = $periode;
            $agreement->opd_initiator   = $opd_initiator;
            $agreement->information     = $information;
            $agreement->due_date        = $due_date;
            $agreement->user_created    = $user_created;

            if($request->hasFile('file')){
                //delete old file
                $path = public_path('storage/documents/collectives/'.$agreement->file);
                Storage::delete($path);

                //add new file
                $files = $_FILES['file']['name'];
                $ext = pathinfo($files, PATHINFO_EXTENSION);
                
                $file_name              = 'collectives_'.time().'.'.$ext;
                $request->file('file')->storeAs('public/documents/collectives', $file_name);
                
                $agreement->file        = $file_name;
            }

            $agreement->save();

            for ($i=0; $i < sizeof($name_of_parties); $i++) { 
                if($name_of_parties[$i] != null)
                {
                    $parties                  = isset($parties_id[$i]) ? Parties::find($parties_id[$i]) : new Parties;
                    $parties->agreement_id    = $agreement->id;
                    $parties->name            = $name_of_parties[$i];
                    $parties->user_created    = $user_created;
                    $parties->save();
                }
            }

        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Kesepakatan bersama berhasil diubah');

        return redirect('agreement/collective');
    }

    public function show($id)
    {
        $data = Agreements::find($id);
        return view('document.collective.show', compact('data'));
    }

    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
            
            Parties::where('agreement_id', $id)->delete();
            Agreements::findOrFail($id)->delete();

        DB::commit();

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Kesepakatan bersama berhasil dihapus');

    	return redirect()->back();
    }
}
