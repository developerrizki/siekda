<?php

namespace App\Http\Requests\Document;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAgreements extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'string',
            'name' => 'string',
            'summary' => 'string',
            'odp_initiator' => 'string',
            'information' => 'string',
            'file' => 'max:3062',
            'due_date' => 'date',
        ];
    }
}
