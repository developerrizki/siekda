<?php

namespace App\Http\Requests\Document;

use Illuminate\Foundation\Http\FormRequest;

class StoreAgreements extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required|string',
            'name' => 'required|string',
            'summary' => 'required|string',
            'opd_initiator' => 'required|string',
            'information' => 'required|string',
            'file' => 'max:3062',
            'due_date' => 'required|date',
        ];
    }
}
