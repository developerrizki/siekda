<?php

namespace App\Http\Requests\Quisioner;

use Illuminate\Foundation\Http\FormRequest;

class StoreQuisionerAgreements extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'agreement_id' => 'required|string',
        ];
    }
}
