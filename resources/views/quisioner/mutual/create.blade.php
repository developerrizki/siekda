@extends('layouts.root')

@section('title','Tambah Kuisioner Perjanjian Kerjasama')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('quisioner/mutual') }}">Kuisioner Perjanjian Kerjasama</a>
        <span class="breadcrumb-item active">Tambah</span>
    </nav>

    @if ($errors->any())
        <div class="alert alert-danger m-t-20">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <form class="form-level" action="{{ url('quisioner/mutual') }}" method="post">
                @csrf
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Tambah Kuisioner Perjanjian Kerjasama</h3>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="material-name">Perjanjian Kerjasama <span class="text-danger">*</span></label>
                                <select name="agreement_id" id="agreement_id" class="form-control js-select2">
                                    <option value="">Pilih Perjanjian Kerjasama</option>
                                    @foreach ($agreements as $key => $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Kuisioner</h3>
                    </div>
                    <div class="block-content">
                        <div id="party" class="party">
                            @foreach ($quisioners as $key => $item)
                                <div class="form-group">
                                    <label for="material-name">{{ $key+1 }}. {{ $item->title }}</label>
                                    <input type="hidden" name="quisioner_id[]" value="{{ $item->id }}">
                                </div>
                                @foreach ($item->poins as $poin)
                                    <div class="form-group">
                                        <input type="radio" name="poin_id[]" value="{{ $poin->id }}"> &nbsp; {{ $poin->poin }}
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                        <hr>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Simpan</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers('select2');
        });

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'agreement_id': { required: true },
            },
            messages: {
                'agreement_id': { required: 'Perjanjian kerjasama harus dipilih' }
            }
        });
    </script>
@endpush
