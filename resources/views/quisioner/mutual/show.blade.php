@extends('layouts.root')

@section('title','Lihat Perjanjian Kerjasama')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('agreement/mutual') }}">Perjanjian Kerjasama</a>
        <span class="breadcrumb-item active">Lihat</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title text-center">
                        DAFTAR INVENTARISASI DATA PERJANJIAN KERJASAMA ANTAR DAERAH
                        PEMERINTAH PROVINSI JAWA BARAT                             
                    </h3>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-sm-12">
                            <label for="material-name">Nama Perjanjian Kerjasama <span class="text-danger">*</span></label>
                            <p>{{ $data->name }}</p>
                        </div>
                        <div class="col-sm-12">
                            <label for="material-name">Nomer <span class="text-danger">*</span></label>
                            <p>{{ $data->number }}</p>
                        </div>
                        <div class="col-sm-12">
                            <label for="material-name">Pihak <span class="text-danger">*</span></label>
                            <ol>
                                @foreach ($data->parties as $key => $item)
                                    <li>{{ $item->name }}</li>
                                @endforeach
                            </ol>
                        </div>
                        <div class="col-sm-12">
                            <label for="material-name">Summary <span class="text-danger">*</span></label>
                            <p>{{ $data->summary }}</p>
                        </div>
                        <div class="col-sm-12">
                            <label for="material-name">Jangka Waktu <span class="text-danger">*</span></label>
                            <p>{{ $data->periode }}</p>
                        </div>
                        <div class="col-sm-12">
                            <label for="material-name">OPD Pemrakarsa <span class="text-danger">*</span></label>
                            <p>{{ $data->opd_initiator }}</p>
                        </div>
                        <div class="col-sm-12">
                            <label for="material-name">Keterangan <span class="text-danger">*</span></label>
                            <p>{{ $data->information }}</p>
                        </div>
                        <div class="col-sm-12">
                            <label for="material-name">Batas Waktu <span class="text-danger">*</span></label>
                            <p>{{ date('d M Y', strtotime($data->due_date)) }}</p>
                        </div>
                        <div class="col-sm-12">
                            <label for="material-name">File Perjanjian Kerjasama</label> <br>
                            <a href="{{ url(Storage::url('documents/mutuals/'.$data->file)) }}" target="_blank">{{ $data->file }}</a>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection