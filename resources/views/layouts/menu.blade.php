<div class="content-side content-side-full">
    <ul class="nav-main">
        @php 
            $user_id = \Auth::user()->level_id;
    
            $role    = App\Model\System\Role::where('users_id', $user_id)
                        ->join('sys_tasks','sys_roles.tasks_id','sys_tasks.tasks_id')
                        ->join('sys_modules','sys_tasks.modules_id','sys_modules.modules_id')
                        ->groupBy('sys_modules.modules_id')
                        ->pluck('sys_modules.modules_id')->toArray();
    
            $menu = App\Model\System\Menu::where('menu_is_sub',0)->orderBy('menu_position','ASC')->get();
        @endphp
        @foreach ($menu as $result)
            @php $check_sub = App\Model\System\Menu::where('menu_parent',$result->menu_id)->count(); @endphp
            @if ($check_sub == 0)
                <li>
                    <a class="{{ Request::is($result->menu_url) || Request::is($result->menu_url.'/*') ? 'active' : '' }}" href="{{ url($result->menu_url) }}"><i class="{{ $result->menu_icon }}"></i> <span class="sidebar-mini-hide">{{ $result->menu }}</span></a>
                </li>
            @else
                @php 
                    $available_sub = App\Model\System\Menu::where('menu_parent', $result->menu_id )  
                        ->whereIn('module_id', $role)
                        ->orderBy('menu_position','ASC')->count(); 
                @endphp
                @if ($available_sub != 0)
                    @php 
                        $sub = App\Model\System\Menu::where('menu_parent', $result->menu_id )  
                            ->whereIn('module_id', $role)
                            ->orderBy('menu_position','ASC')->get(); 
                    @endphp
                    <li class="">
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="{{ $result->menu_icon }}"></i> <span class="sidebar-mini-hide">{{ $result->menu }}</span></a>
                        <ul>
                            @foreach ($sub as $item)
                                <li><a class="{{ Request::is($item->menu_url) || Request::is($item->menu_url.'/*') ? 'active' : '' }}" href="{{ url($item->menu_url) }}">{{ $item->menu }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                @endif
            @endif
        @endforeach
    </ul>
</div>
