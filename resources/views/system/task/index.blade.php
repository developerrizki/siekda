@extends('layouts.root')

@section('title','Task')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Task</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

    	@if(Session::has('status'))
    		@if(Session::get('status') == '200')
    	        <div class="alert alert-success alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Success!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @elseif(Session::get('status') == 'err')
    			<div class="alert alert-danger alert-dismissible" role="alert">
    	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	            <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Failed!</h3>
    	            <p class="mb-0">{{ Session::get('msg') }}</p>
    	        </div>
            @endif
        @endif

        <h2 class="content-heading">
            <a class="btn btn-sm btn-warning float-right" href="{{ url('system/task/create') }}">
                <i class="fa fa-plus"></i> &nbsp; Add New Task
            </a>
            Task
        </h2>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content">
                <table class="table table-vcenter table-stripped">
                    <thead>
                        <tr>
                            <th style="width: 50px;">No</th>
                            <th>Module</th>
                            <th>Task</th>
                            <th style="width: 200px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($task) > 0)
                            @foreach($task as $result)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $result->module->module }}</td>
                                    <td>{{ $result->tasks }}</td>
                                    <td>
                                        <button  data-toggle="tooltip" title="Hapus" data-action="{{ url('system/task/delete/'. $result->tasks_id) }}" class="btn btn-sm btn-danger btn-xs btn-delete">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
						@else
							<tr>
								<td colspan='4'>Data Tidak Tersedia</td>
							</tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Table -->
    </div>


</div>

@include('modal_delete')
@endsection

@push('script')
    <!-- DataTables -->
    <script src="{{ asset('themes/plugins/datatables/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('themes/plugins/datatables/dataTables.bootstrap.js') }} "></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').dataTable({
            	"pageLength": 50,
            	"lengthChange": false,
            	"searching": false
            });
        } );

        $('.btn-delete').click(function(){
        	var action = $(this).data('action');

        	$('#modalDelete').modal('show');
        	$('.modal-title').html('Delete Level');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
