@extends('layouts.root')

@section('title','Ubah Perjanjian Kerjasama')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('agreement/mutual') }}">Perjanjian Kerjasama</a>
        <span class="breadcrumb-item active">Ubah</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <form class="form-level" action="{{ url('agreement/mutual/update/'.$data->id) }}" method="post">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Ubah Perjanjian Kerjasama</h3>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="material-name">Nama Perjanjian Kerjasama <span class="text-danger">*</span></label>
                                <textarea name="name" id="name" cols="30" rows="10" class="form-control">{{ $data->name }}</textarea>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="material-name">Nomer <span class="text-danger">*</span></label>
                                <input type="text" autocomplete="off" class="form-control" id="number" name="number" value="{{ $data->number }}" placeholder="Masukan nomer perjanjian kerjasama">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="material-name">Summary <span class="text-danger">*</span></label>
                                <textarea name="summary" id="summary" cols="30" rows="10" class="form-control">{{ $data->summary }}</textarea>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="material-name">Jangka Waktu <span class="text-danger">*</span></label>
                                <textarea name="periode" id="periode" cols="30" rows="5" class="form-control">{{ $data->periode }}</textarea>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="material-name">OPD Pemrakarsa <span class="text-danger">*</span></label>
                                <textarea name="opd_initiator" id="opd_initiator" cols="30" rows="2" class="form-control">{{ $data->opd_initiator }}</textarea>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="material-name">Keterangan <span class="text-danger">*</span></label>
                                <textarea name="information" id="information" cols="30" rows="10" class="form-control">{{ $data->information }}</textarea>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="material-name">Batas Waktu <span class="text-danger">*</span></label>
                                <input type="text" autocomplete="off" class="form-control" id="due_date" name="due_date" placeholder="Masukan batas waktu" value="{{ $data->due_date }}">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="material-name">File Perjanjian Kerjasama</label> <br>
                                <input type="file" name="file" id="file"> <br><br>
                                <span>File Dokumen: </span>
                                <a href="{{ url(Storage::url('documents/mutuals/'.$data->file)) }}" target="_blank">{{ $data->file }}</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Para pihak</h3>
                    </div>
                    <div class="block-content">
                        <div id="party" class="party">
                            @foreach ($data->parties as $key => $item)
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label for="material-name">Nama <span class="text-danger">*</span></label>
                                            <input type="text" autocomplete="off" class="form-control" name="name_of_parties[]" placeholder="Masukan nama para pihak" value="{{ $item->name }}" required>
                                            <input type="hidden" name="parties_id[]" value="{{ $item->id }}">
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <hr>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Simpan</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                    </div>
                </div>
                
            </form>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers('select2');
        });

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'name': { required: true },
                'number': { required: true },
                'summary': { required: true },
                'periode': { required: true },
                'opd_initiator': { required: true },
                'information': { required: true },
                'due_date': { required: true }
            },
            messages: {
                'name': { required: 'Nama perjanjian kerjasama harus diisi' },
                'number': { required: 'Nomer perjanjian kerjasama harus diisi' },
                'summary': { required: 'Summary harus diisi' },
                'periode': { required: 'Jangka waktu harus diisi' },
                'opd_initiator': { required: 'OPD pemrakarsa harus diisi' },
                'information': { required: 'Keterangan harus diisi' },
                'due_date': { required: 'Batas waktu harus diisi' }
            }
        });
    </script>
@endpush
