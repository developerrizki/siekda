@extends('layouts.root')

@section('title','Kesepakatan Bersama')

@section('content')
<div class="container">
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Kesepakatan Bersama</span>
        </nav>

        @if ($errors->any())
    	    <div class="alert alert-danger m-t-20">
    	        <ul>
    	            @foreach ($errors->all() as $error)
    	                <li>{{ $error }}</li>
    	            @endforeach
    	        </ul>
    	    </div>
    	@endif

        @if(Session::has('status'))
            @if(Session::get('status') == '200')
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Berhasil!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @elseif(Session::get('status') == 'err')
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h3 class="alert-heading font-size-h4 font-w400"><i class="icon fa fa-check"></i> Gagal!</h3>
                    <p class="mb-0">{{ Session::get('msg') }}</p>
                </div>
            @endif
        @endif

        <h2 class="content-heading">
            <a class="btn btn-sm btn-warning float-right" href="{{ url('agreement/collective/create') }}">
                <i class="fa fa-plus"></i> &nbsp; Tambah Kesepakatan Bersama
            </a>
            Kesepakatan Bersama
        </h2>


        <!-- Filter -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Pencarian</h3>
            </div>
            <div class="block-content">
                <form action="{{ url('agreement/collective') }}" class="form-inline" method="GET">
                    <div class="form-group" style="width: 25%">
                        <label for="">Berdasarkan</label>
                        <select name="key" id="key" class="form-control" style="width: 100%">
                            <option value="">Cari Berdasarkan</option>
                            <option value="number" @if(Request::get("key") == 'title') selected @endif>Nomor</option>
                            <option value="name" @if(Request::get("key") == 'name') selected @endif>Nama Perjanjian</option>
                            <option value="periode" @if(Request::get("key") == 'periode') selected @endif>Jangka Waktu</option>
                            <option value="opd_initiator" @if(Request::get("key") == 'opd_initiator') selected @endif>ODP Pemrakarsa</option>
                            <option value="information" @if(Request::get("key") == 'information') selected @endif>Keterangan</option>
                        </select>
                    </div>
                    <div class="form-group" style="margin-left:10px; width: 51%">
                        <label for="">Pencarian</label>
                        <input type="text" autocomplete="off" class="form-control" id="value" name="value" value="{{ Request::get('value') }}" style="width: 100%" placeholder="Masukan data pencarian">
                    </div>
                    <div class="form-group" style="margin:20px 0 0 10px; width: 21.5%">
                        <button class="btn btn-primary" style="width: 100%"> <i class="fa fa-search"></i> Cari</button>
                    </div>
                </form> <br>
            </div>
        </div>

        <!-- Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Table</h3>
            </div>
            <div class="block-content">
                <div class="table-responsive">
                    <table class="table table-vcenter table-stripped" style="font-size: 11px;">
                        <thead>
                            <tr>
                                <th style="width: 50px;">No</th>
                                <th>Nama Perjanjian</th>
                                <th>Nomer</th>
                                <th width="11.5%">Batas Waktu</th>
                                <th>Pembuat</th>
                                <th style="width: 150px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($data) > 0)
                                @foreach($data as $key => $result)
                                    @php
                                        $date1  = date_create($result->due_date);
                                        $date2  = date_create(date('Y-m-d'));
                                        $diff   = date_diff($date2,$date1);

                                        $duration = $diff->format("%a");
                                        $ev = $diff->format("%R");
                                        $style    = '';

                                        if( $result->status == 0 && ($duration > 160 && $duration <= 360) ){
                                            $style = 'background-color: #ebf5df';
                                        }elseif( $result->status == 0 && ($duration > 60 && $duration <= 160) ) {
                                            $style = 'background-color: #fcf7e6';
                                        }elseif( $result->status == 0 && ($duration <= 60 || $ev == '-') ){
                                            $style = 'background-color: #fae9e8';
                                        }
                                        
                                    @endphp
                                    <tr style="{{ $style }}">
                                        <td>{{ ($data->currentpage()-1) * $data->perpage() + $key + 1 }}.</td>
                                        <td>{{ $result->name }}</td>
                                        <td>{{ $result->number }}</td>
                                        <td>{{ date('d M Y', strtotime($result->due_date)) }}</td>
                                        <td>{{ $result->user->name }}</td>
                                        <td>
                                            <a href="{{ url('agreement/collective/edit/'. $result->id) }}" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Ubah">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <a href="{{ url('agreement/collective/show/'. $result->id) }}" class="btn btn-sm btn-success" data-toggle="tooltip" title="Detail">
                                                <i class="fa fa-search"></i>
                                            </a>
                                            <button type="button" data-toggle="tooltip" title="Hapus" data-action="{{ url('agreement/collective/delete/'. $result->id) }}" class="btn btn-sm btn-danger btn-xs btn-delete">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="7" class="text-center">Data Kesepakatan Bersama tidak tersedia.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Table -->
        {{ $data->links() }}
    </div>


</div>
@include('modal_delete')
@endsection

@push('script')

    <script type="text/javascript">

        $('.btn-delete').click(function(){
        	var action = $(this).data('action');

        	$('#modalDelete').modal('show');
        	$('.modal-title').html('Delete Kesepakatan Bersama');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
