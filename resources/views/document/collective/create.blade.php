@extends('layouts.root')

@section('title','Tambah Kesepakatan Bersama')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('agreement/collective') }}">Kesepakatan Bersama</a>
        <span class="breadcrumb-item active">Tambah</span>
    </nav>

    @if ($errors->any())
        <div class="alert alert-danger m-t-20">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <form class="form-level" action="{{ url('agreement/collective') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Tambah Kesepakatan Bersama</h3>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="material-name">Nama Kesepakatan Bersama <span class="text-danger">*</span></label>
                                <textarea name="name" id="name" cols="30" rows="10" class="form-control"></textarea>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="material-name">Nomer <span class="text-danger">*</span></label>
                                <input type="text" autocomplete="off" class="form-control" id="number" name="number" placeholder="Masukan nomer Kesepakatan Bersama">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="material-name">Summary <span class="text-danger">*</span></label>
                                <textarea name="summary" id="summary" cols="30" rows="10" class="form-control"></textarea>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="material-name">Jangka Waktu <span class="text-danger">*</span></label>
                                <textarea name="periode" id="periode" cols="30" rows="5" class="form-control"></textarea>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="material-name">OPD Pemrakarsa <span class="text-danger">*</span></label>
                                <textarea name="opd_initiator" id="opd_initiator" cols="30" rows="2" class="form-control"></textarea>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="material-name">Keterangan <span class="text-danger">*</span></label>
                                <textarea name="information" id="information" cols="30" rows="10" class="form-control"></textarea>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="material-name">Batas Waktu <span class="text-danger">*</span></label>
                                <input type="text" autocomplete="off" class="form-control" id="due_date" name="due_date" placeholder="Masukan batas waktu">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="material-name">File Kesepakatan Bersama</label> <br>
                                <input type="file" name="file" id="file">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Para pihak</h3>
                        <div class="text-right">
                            <button type="button" class="btn btn-success btn-sm btn-duplicate-poin">
                                <i class="fa fa-plus"></i> Tambah pihak
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div id="party" class="party">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-11">
                                        <label for="material-name">Nama <span class="text-danger">*</span></label>
                                        <input type="text" autocomplete="off" class="form-control" name="name_of_parties[]" placeholder="Masukan nama para pihak" required>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                            </div>
                        </div>
                        <div class="party-group"></div>
                        <hr>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Simpan</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers('select2');
            
            var cloneCount = 1;

            $('.btn-duplicate-poin').bind('click', function(){
                var ids = cloneCount++;

                $( ".party .form-group" ).clone()
                                        .attr('id','party'+ ids)
                                        .find("input:text").val("").end()
                                        .appendTo( ".party-group" );
                $('#party'+ ids +' .col-sm-1').html('<button type="button" class="btn btn-danger" style="position: relative; top: 25px;" onclick=removeClone('+ ids +')><i class="fa fa-trash"></button>');
            });
        });

        
        function removeClone(id)
        {
            $('#party'+id).remove();
        }

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'name': { required: true },
                'number': { required: true },
                'summary': { required: true },
                'periode': { required: true },
                'opd_initiator': { required: true },
                'information': { required: true },
                'due_date': { required: true }
            },
            messages: {
                'name': { required: 'Nama Kesepakatan Bersama harus diisi' },
                'number': { required: 'Nomer Kesepakatan Bersama harus diisi' },
                'summary': { required: 'Summary harus diisi' },
                'periode': { required: 'Jangka waktu harus diisi' },
                'opd_initiator': { required: 'OPD pemrakarsa harus diisi' },
                'information': { required: 'Keterangan harus diisi' },
                'due_date': { required: 'Batas waktu harus diisi' }
            }
        });
    </script>
@endpush
