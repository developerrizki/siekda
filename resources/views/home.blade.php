@extends('layouts.root')

@section('title','Dashboard')

@section('content')
    <!-- Page Content -->
    <div class="content">
        <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
            <span class="breadcrumb-item active">Admin</span>
        </nav>
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title"><small>Get Started</small></h3>
            </div>
            <div class="block-content">
                <p>You are logged in!</p>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection
