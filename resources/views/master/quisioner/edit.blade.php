@extends('layouts.root')

@section('title','Ubah Kuisioner')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('quisioner') }}">Kuisioner</a>
        <span class="breadcrumb-item active">Ubah</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <form class="form-level" action="{{ url('quisioner/update/'.$data->id) }}" method="post">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Ubah Kuisioner</h3>
                    </div>
                    <div class="block-content">
                        <div class="form-group">
                            <div class="form-material">
                            <input type="text" autocomplete="off" class="form-control" id="title" name="title" placeholder="Masukan pertanyaan kuisioner" value="{{ $data->title }}">
                                <label for="material-name">Pertanyaan <span class="text-danger">*</span></label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Poin - poin kuisioner baru</h3>
                    </div>
                    <div class="block-content">
                        <div id="poins" class="poins">
                            @foreach ($data->poins as $key => $item)
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label for="material-name">Poin <span class="text-danger">*</span></label>
                                            <input type="text" autocomplete="off" class="form-control" name="poin[]" placeholder="Contoh : Cukup, Baik, Sangat Baik" value="{{ $item->poin }}" required>
                                            <input type="hidden" name="poin_id[]" value="{{ $item->id }}">
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <hr>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Simpan</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers('select2');
        });

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'title': {
                    required: true,
                    minlength: 3
                }
            },
            messages: {
                'title': {
                    required: 'Inputan pertanyaan harus diisi',
                    minlength: 'Isian pertanyaan minimal terdiri dari 3 karakter atau lebih'
                }
            }
        });
    </script>
@endpush
