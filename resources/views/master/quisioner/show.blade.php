@extends('layouts.root')

@section('title','Lihat Kuisioner')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('quisioner') }}">Kuisioner</a>
        <span class="breadcrumb-item active">Lihat</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Data Kuisioner</h3>
                </div>
                <div class="block-content">
                    <label for="material-name">Pertanyaan</label>
                    <p>{{ $data->title }}</p>
                </div>
            </div>

            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Poin Kuisioner</h3>
                </div>
                <div class="block-content">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <th>Poin</th>
                            <th>Pembuat</th>
                            <th>Jumlah</th>
                        </thead>
                        @foreach ($data->poins as $key => $item)
                            <tr>
                                <td>{{ $item->poin }}</td>
                                <td>{{ $data->user->name }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers('select2');
        });
        
        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'title': {
                    required: true,
                    minlength: 3
                }
            },
            messages: {
                'title': {
                    required: 'Inputan pertanyaan harus diisi',
                    minlength: 'Isian pertanyaan minimal terdiri dari 3 karakter atau lebih'
                }
            }
        });
    </script>
@endpush
