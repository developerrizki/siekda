@extends('layouts.root')

@section('title','Tambah Kuisioner')

@section('content')

<!-- Page Content -->
<div class="content">

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dashboard</a>
        <a class="breadcrumb-item" href="{{ url('quisioner') }}">Kuisioner</a>
        <span class="breadcrumb-item active">Tambah</span>
    </nav>
    <!-- Material Design -->
    <div class="row">
        <div class="col-md-12">
            <!-- Static Labels -->
            <form class="form-level" action="{{ url('quisioner') }}" method="post">
                @csrf
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Tambah Kuisioner Baru</h3>
                    </div>
                    <div class="block-content">
                        <div class="form-group">
                            <div class="form-material">
                                <input type="text" autocomplete="off" class="form-control" id="title" name="title" placeholder="Masukan pertanyaan kuisioner">
                                <label for="material-name">Pertanyaan <span class="text-danger">*</span></label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Poin - poin kuisioner baru</h3>
                        <div class="text-right">
                            <button type="button" class="btn btn-success btn-sm btn-duplicate-poin">
                                <i class="fa fa-plus"></i> Tambah poin
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div id="poins" class="poins">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-11">
                                        <label for="material-name">Poin <span class="text-danger">*</span></label>
                                        <input type="text" autocomplete="off" class="form-control" name="poin[]" placeholder="Contoh : Cukup, Baik, Sangat Baik" required>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                            </div>
                        </div>
                        <div class="poin-group"></div>
                        <hr>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">Simpan</button>
                            <button type="reset" class="btn btn-secondary">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END Static Labels -->
        </div>
        <!-- END column -->
    </div>
    <!-- END row -->
</div>
<!-- END content -->

@endsection

@push('script')

    <script src="{{ asset('assets/js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery-validation/additional-methods.min.js') }}"></script>
    <script>
        jQuery(function () {
            // Init page helpers (Select2 plugin)
            Codebase.helpers('select2');
            
            var cloneCount = 1;

            $('.btn-duplicate-poin').bind('click', function(){
                var ids = cloneCount++;

                $( ".poins .form-group" ).clone()
                                        .attr('id','poins'+ ids)
                                        .find("input:text").val("").end()
                                        .appendTo( ".poin-group" );
                $('#poins'+ ids +' .col-sm-1').html('<button type="button" class="btn btn-danger" style="position: relative; top: 25px;" onclick=removeClone('+ ids +')><i class="fa fa-trash"></button>');
            });
        });

        
        function removeClone(id)
        {
            $('#poins'+id).remove();
        }

        $('.form-level').validate({
            ignore: [],
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: function(error, e) {
                jQuery(e).parents('.form-group').append(error);
            },
            highlight: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: function(e) {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'title': {
                    required: true,
                    minlength: 3
                }
            },
            messages: {
                'title': {
                    required: 'Inputan pertanyaan harus diisi',
                    minlength: 'Isian pertanyaan minimal terdiri dari 3 karakter atau lebih'
                }
            }
        });
    </script>
@endpush
